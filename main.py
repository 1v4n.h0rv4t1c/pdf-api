from fastapi import FastAPI, HTTPException, Depends, status
from fastapi.security import HTTPBasic, HTTPBasicCredentials
from starlette.responses import FileResponse
import os
import secrets

app = FastAPI()

security = HTTPBasic()

def verify_password(username: str, password: str):
    with open('passwords.txt', 'r') as f:
        for line in f:
            user, pwd = line.strip().split(':')
            if user == username and pwd == password:
                return True
    return False

@app.get("/schemas/{client}/{filename}")
async def read_file(client: str, filename: str, credentials: HTTPBasicCredentials = Depends(security)):
    if not verify_password(credentials.username, credentials.password) or credentials.username != client:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Basic"},
        )

    file_path = f"schemas/{client}/{filename}"
    if os.path.exists(file_path):
        return FileResponse(file_path)
    else:
        raise HTTPException(status_code=404, detail="File not found")

